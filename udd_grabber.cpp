#include <iostream>
#include <fstream>
#include <string>
#include <cstddef>
#include <chrono>
#include <ctime>
#include <vector>

void show_usage(const std::string& name) {
    std::cerr << "Usage: " << name << " <term>\n"
              << "Options: \n"
              << "\t-h, --help             | Shows this help message\n"
              << std::endl;
}

const std::string NO_DEFINITION_FOUND = "[ERROR] No definition found or no internet connection could be established!";

void replace_all_occurences(std::string& data, std::string sub_string, std::string replacement) {
    // Get position of the first occurence of "sub_string"
    std::size_t pos = data.find(sub_string);
    // Loop as long as "sub_string" is found
    while (pos != std::string::npos) {
        data.replace(pos, sub_string.length(), replacement);
        // Start search of next possible occurence at last found position + length() of the replacement
        pos = data.find(sub_string, pos + replacement.length());
    }
}

int biggest_number(const std::vector<int>& numbers) {
    int biggest_number = numbers[0];
    for (int i : numbers) {
        if (i > biggest_number) {
            biggest_number = i;
        }
    }
    
    return biggest_number;
}

int get_longest_line_length(const std::string& s) {
    int tmp_line_length = 0;
    int line_length = 0;
    // Loop through description
    if (s.find('\n') != std::string::npos) {
        for (char c : s) {
            if (c == '\n') {
                if (tmp_line_length > line_length) {
                    line_length = tmp_line_length;
                    tmp_line_length = 0;
                } else {
                    tmp_line_length = 0;
                }
            } else {
                tmp_line_length++;
            }
        }
    } else {
        line_length = s.length();
    }
    
    return line_length;
}

bool is_string_equal_to_sequence(const std::string& sub_string, int start_sequence, const std::string& line) {
    std::string sequence = "";
    // Generate sequence
    for (std::size_t i = start_sequence; i < start_sequence + sub_string.length(); i++) {
        sequence += line[i];
    }
    
    return sub_string == sequence;
}

std::pair<std::string, int> extract_auto_link_text(int start_auto_link, const std::string& line, const std::string& closing_tag) {
    std::string auto_link_text = "";
    std::pair<std::string, int> auto_link_text_and_skip_count;
    
    /* 
    Start of the auto_link_text gets found, 
    Ex. (Start of "auto_link_text" is at index 17) "<div class="...">...<div/>"
    */
    std::size_t start_auto_link_text = line.find(">", start_auto_link);
    /* 
    End of the auto_link_text gets found (Beginning at the start of the auto_link_text), 
    Ex. (End of "auto_link_text" is at index 21) "<div class="...">...<div/>"
    */
    std::size_t end_auto_link_text = line.find(closing_tag, start_auto_link_text);
    
    std::size_t end_auto_link;
    
    int skip_count;
    
    // Adding 1 to exclude ">"
    // "<" and not "<=" to exclude "<"
    for (std::size_t i = start_auto_link_text + 1; i < end_auto_link_text; i++) {
        // Adding each char between ">" and "<" to "auto_link_text"
        auto_link_text += line[i];
        // Setting "end_auto_link" every iteration to get the LAST "i" + "closing_tag.length()" + 1  
        end_auto_link = i + closing_tag.length() + 1;
    }
    
    // Set skip count to skip the whole auto_link (<a>...</a>)
    skip_count = end_auto_link - start_auto_link;
    
    // Set pair
    auto_link_text_and_skip_count.first = auto_link_text;
    auto_link_text_and_skip_count.second = skip_count;
    
    return auto_link_text_and_skip_count;
}

std::string extract_html_class_text(const std::string& term, const std::string& html_class) {
    std::string start_description = "<div class=\"" + html_class +"\">";
    std::string description = "";
    std::string line;
    
    
    // Only download when the meaning is extracted, because this is the first one and more downloads are not needed.
    if (html_class == "meaning") {
         std::string url_compatible_term = term;
        replace_all_occurences(url_compatible_term, " ", "%20");
        
        std::string command = "wget -q -O /tmp/udd_grabber_index.html https://www.urbandictionary.com/define.php?term=" + url_compatible_term;
        system(command.c_str());
    }
   
    
    std::ifstream file("/tmp/udd_grabber_index.html");
    
    if (file.peek() == std::ifstream::traits_type::eof()) {
        return NO_DEFINITION_FOUND;
    }
    
    int word_count = 0;
    std::pair<std::string, int> auto_link_text_and_skip_count;
    
    while (std::getline(file, line)) {
        std::size_t pos_start_description = line.find(start_description);
        std::size_t pos_start_auto_link_text;
        
        if (pos_start_description != std::string::npos) {
            for (std::size_t i = pos_start_description + start_description.length(); i < line.length(); i++) {
                if (is_string_equal_to_sequence("</div>", i, line)) {
                    replace_all_occurences(description, "&apos;", "'");
                    replace_all_occurences(description, "&quot;", "\"");
                    replace_all_occurences(description, "<br/>", "");
                    return description;
                } else if (is_string_equal_to_sequence("<br/>", i, line)) {
                    description += "\n";
                    word_count = 0;
                } else if (is_string_equal_to_sequence("<a", i, line)) {
                    auto_link_text_and_skip_count = extract_auto_link_text(i, line, "</a>");
                    
                    description += auto_link_text_and_skip_count.first;
                    i += auto_link_text_and_skip_count.second;
                    
                    
                    // This fixes a problem of div tags occuring when the description consists of only one auto link.
                    // This checks if the next symbol is a closing tag ("/") and returns the description when yes.
                    if (line[i + 1] == '/' && line[i] == '<') {
                        replace_all_occurences(description, "&apos;", "'");
                        replace_all_occurences(description, "&quot;", "\"");
                        replace_all_occurences(description, "<br/>", "");
                        return description;
                    }
                } 
                
                description += line[i];
                // Managing appropriate line breaks
                if (line[i] == ' ') {
                    word_count++;
                    if ((word_count % 15) == 0) {
                        description += "\n";
                    }
                }
            }
        }
    }
    return "";
}

void display_result(const std::string& term) {
    std::string extracted_description = extract_html_class_text(term, "meaning");
    std::string extracted_example = extract_html_class_text(term, "example");
    std::string extracted_contributor = extract_html_class_text(term, "contributor");
//     std::string extracted_upvotes = extract_html_class_text(term, "count");
    
    if (extracted_description == NO_DEFINITION_FOUND) {
        std::cout << NO_DEFINITION_FOUND << std::endl;
        return;
    }
    
    int longest_line_length_description = get_longest_line_length(extracted_description);
    int longest_line_length_example = get_longest_line_length(extracted_example);
    int longest_line_length_term = term.length();
    int longest_line_length_contributor = get_longest_line_length(extracted_contributor);
    
    std::vector<int> numbers = { 
        longest_line_length_description, 
        longest_line_length_example, 
        longest_line_length_term, 
        longest_line_length_contributor 
    };
    
    std::string seperator = std::string(biggest_number(numbers), '-');
    
    std::cout << term << std::endl;
    std::cout << seperator << std::endl;
    
    std::cout << extracted_description << std::endl;
    std::cout << seperator << std::endl;
    
    std::cout << extracted_example << std::endl;
    std::cout << seperator << std::endl;
    
    std::cout << extracted_contributor << std::endl;
//     std::cout << seperator << std::endl;
//     
//     std::cout << extracted_upvotes << std::endl;
}

void parse_arguments(int argc, char* argv[]) {
    const int MIN_ARGUMENT_COUNT = 1;
    const int MAX_ARGUMENT_COUNT = 2;
    
    std::string term;
    
    if (argc >= MIN_ARGUMENT_COUNT && argc <= MAX_ARGUMENT_COUNT) {
        // Called with no arguments
        if (argc == 1) {
            std::cin >> term;
            display_result(term);
        } else {
            // Loop through arguments skipping argv[0] by starting at "i = 1", because this is the name with which the program was called.
            for (std::size_t i = 1; i < argc; i++) {
                if (std::string(argv[i]) == "-h" || std::string(argv[i]) == "--help") {
                    show_usage(argv[0]);
                    break;
                } else if (argc == 2) {
                    term = argv[i];
                    display_result(term);
                }
            }
        }
    } else {
        show_usage(argv[0]);
    }
}

int main(int argc, char* argv[]) {
    parse_arguments(argc, argv);
    return 0;
}
